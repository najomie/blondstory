<?php
/**
 * Plugin Name: CPT Collaborateurs
 * Plugin URI: http://najomie.com
 * Description: CPT Collaborateurs
 * Version: 1.0.0
 * Author: Najomie
 * Author URI: http://najomie.com
 * Requires at least: 4.0
 * Tested up to: 4.3
 *
 * @package CPT_COLLABORATEUR
 * @category Core
 * @author NAJOMIE
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'CPT_COLLABORATEUR' ) ) {

class CPT_COLLABORATEUR
{
	const DEBUG = FALSE;

	/**
	 * Plugin version, match with plugin header
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * Use the function not the variable
	 * @var string
	 */
	public $plugin_url;

	/**
	 * Use the function not the variable
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Do we update the rewrite rules for a custom post type?
	 * @var boolean
	 */
	public $flush_rules = FALSE;

	/**
	 * PLUGIN STARTUP
	 */
	public function __construct(){
		// do something when we activate/deactivate the plugin
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		$installed = get_option( 'CPT_COLLABORATEUR_Version', FALSE );

		if( ! $installed or version_compare($installed, $this->version, '!=') ){
			$this->flush_rules = TRUE;
			update_option( 'CPT_COLLABORATEUR_Version', $this->version );
		}

		$this->hooks();
	}

	/**
	 * Register the plugin's hooks
	 */
	public function hooks(){
		add_action( 'init', array($this, 'init'), 0 );
	}

	/**
	 * Runs on WordPress init hook
	 */
	public function init(){
		$this->post_types();
		$this->flush();
	}

	/**
	 * Register the post types and taxonomies
	 *
	 * @link https://developer.wordpress.org/resource/dashicons/ for admin menu icons
	 */
	 public function post_types(){
		 $this->CPT_COLLABORATEUR();
	}

	public function CPT_COLLABORATEUR(){

		$labels = array(
			'name' => _x( 'Collaborateurs', 'collaborateur' ),
			'singular_name' => _x( 'Collaborateur', 'collaborateur' ),
			'add_new' => _x( 'Ajouter un collaborateur', 'collaborateur' ),
			'add_new_item' => _x( 'Ajouter un nouveau collaborateur', 'collaborateur' ),
			'edit_item' => _x( 'Modifier le collaborateur', 'collaborateur' ),
			'new_item' => _x( 'Nouveau collaborateur', 'collaborateur' ),
			'view_item' => _x( 'Afficher le collaborateur', 'collaborateur' ),
			'search_items' => _x( 'Rechercher dans les collaborateurs', 'collaborateur' ),
			'not_found' => _x( 'Aucun collaborateur trouvé', 'collaborateur' ),
			'not_found_in_trash' => _x( 'Aucun collaborateur trouvé dans la corbeille', 'collaborateur' ),
			'parent_item_colon' => _x( 'Collaborateur parent:', 'collaborateur' ),
			'menu_name' => _x( 'Collaborateurs', 'collaborateur' ),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array( 'title' ),
			'taxonomies' => array(),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,

			'menu_icon' => 'dashicons-admin-users',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'capability_type' => 'post'
		);

		register_post_type( 'collaborateur', $args );
	}


	/**
	 * Refresh rewrite rules
	 */
	public function flush(){
		if( $this->flush_rules )
			flush_rewrite_rules();
	}

	public function activate(){
		$this->flush_rules = TRUE; // we will need to refresh the rewrite rules for the custom post types
	}

	public function deactivate(){
		flush_rewrite_rules();
	}

	public function imgURL( $file ){
		return $this->plugin_url() . "/assets/images/{$file}";
	}

	public function jsURL( $file ){
		return $this->plugin_url() . "/assets/js/{$file}";
	}
	public function jsPATH( $file ){
		return $this->plugin_path() . "/assets/js/{$file}";
	}

	public function cssURL( $file ){
		return $this->plugin_url() . "/assets/css/{$file}";
	}
	public function cssPATH( $file ){
		return $this->plugin_path() . "/assets/css/{$file}";
	}

	/**
	 * Get the plugin url.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_url() {
		if ( $this->plugin_url ) return $this->plugin_url;
		return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_path() {
		if ( $this->plugin_path ) return $this->plugin_path;
		return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}
}

// Init Class and register in global scope
$GLOBALS['CPT_COLLABORATEUR'] = new CPT_COLLABORATEUR();

} // class_exists check
