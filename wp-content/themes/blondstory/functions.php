<?php

/**
 * NAJOMIE main theme functions
 *
 * @author Gabriel Gaudreau
 */

class NAJ_ThemeFunctions
{

	/**
	 * Theme Version, change to refresh css and js browser cache
	 * @var string
	 */
	public $version = '1.3.0';

	/**
	 * BOOT
	 * Set some constants and startup
	 */
	public function __construct(){
		define( 'NAJ_THEME_VERSION', $this->version );
		define( 'NAJ_THEME_MMENU', FALSE );
		define( 'NAJ_ANIMATESCROLL', FALSE );

		$this->actions();
	}

	/**
	 * Main Hooks
	 */
	public function actions(){
		// MAIN ACTIONS
		add_action( 'init', array($this, 'init') );
		add_action( 'after_setup_theme', array($this, 'after_setup_theme') );
		add_action( 'widgets_init', array($this, 'widgets_init') );
		add_action( 'wp_head', array($this, 'wp_head'), 1 );
		add_action( 'wp_enqueue_scripts', array($this, 'wp_enqueue_scripts') );

		// POST FILTERS
		add_filter( 'post_class', array($this, 'post_class') );
		add_filter( 'sanitize_file_name', array($this, 'sanitize_filename_on_upload'), 10 );
		add_filter( 'body_class', array($this, 'custom_taxonomy_in_body_class') );
		remove_filter( 'the_content', 'easy_image_gallery_append_to_content' );

		// GRAVITY FORMS
		add_filter( 'gform_custom_merge_tags', array($this, 'custom_merge_tags'), 10, 4 );
		add_filter( 'gform_replace_merge_tags', array($this, 'replace_custom_merge_tags'), 10, 7 );

		// MAKE SURE WPSEO METABOX IS AT THE BOTTOM
		add_filter( 'wpseo_metabox_prio', function(){ return 'low'; } );

		$this->shortcodes();
		$this->customize_login();
	}

	public function init(){
		$this->menus();
		$this->sidebars();
	}

	/**
	 * Register theme menu locations
	 */
	public function menus(){
		register_nav_menus( array(
			'top-nav'   => __( 'Top menu', 'theme' ),
			'main-nav'   => __( 'Navigation principale', 'theme' ),
			'mobile-nav' => __( 'Navigation mobile', 'theme' ),
		));
	}

	/**
	 * Register theme sidebar locations for widgets
	 */
	public function sidebars(){
		register_sidebar(array(
			'id' 			=> 'blog-sidebar',
			'name' 			=> 'Sidebar blogue',
			'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<h3>',
			'after_title' 	=> '</h3>',
		));
	}

	/**
	 * Define new shortcodes
	 */
	public function shortcodes(){
		add_shortcode( 'email', array($this, 'sc_hide_email') );
		add_shortcode( 'custom-field', array($this, 'sc_custom_field') );
		add_shortcode( 'ptb-footer-lock', array($this, 'sc_footer_lock') );
		add_shortcode( 'google_map', array($this, 'google_map') );
		add_shortcode( 'pagebanner', array($this, 'pagebanner') );
		add_shortcode( 'homeblocs', array($this, 'homeblocs') );
		add_shortcode( 'newsletter', array($this, 'newsletter') );
		add_shortcode( 'videogrid', array($this, 'videogrid') );
		add_shortcode( 'publicite', array($this, 'sc_pubs') );
		add_shortcode( 'gridevents', array($this, 'gridevents') );
		add_shortcode( 'pub', array($this, 'single_pub') );
	}

	/**
	 * Change login logo and logo link and maybe design aswell
	 */
	public function customize_login(){
		add_action( 'login_head', array($this, 'login_logo') );
		add_filter( 'login_headerurl', array($this, 'login_url') );
	}

	public function login_logo(){ ?>
		<style>
	    .login h1 a { background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/logo-najomie.png); width: 175px; height: 175px; background-size: 175px 175px; margin: 10px auto; }
		</style>
	<?php }

	public function login_url(){
		return 'http://najomie.com';
	}

	/**
	 * Hook run after theme is setup
	 */
	public function after_setup_theme(){
		// add support for post thumbnails
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'post-list-thumbnail', 480, 300, TRUE );
		add_image_size( 'home-bloc', 370, 240, TRUE );
		add_image_size( 'single-post-image', 1170, 360, TRUE );
	}

	/**
	 * Hook run once widgets are ready
	 */
	public function widgets_init(){
		// prevent visual composer from checking for updates which creates errors
		wp_clear_scheduled_hook('wpb_check_for_update');
	}

	/**
	 * Output extra data to html's head
	 *
	 * @since revision 24: Removed maximum-scale=1.0, user-scalable=no
	 */
	public function wp_head(){
		global $is_IE;

		if( $is_IE ){ ?>
			<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
		<?php } ?>

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php

		$this->favicons();
	}

	/**
	 * Output favicon html in html's head
	 *
	 * @see wp_head()
	 */
	public function favicons(){
		?>
		<link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png?v=eEEKWE20Q4">
		<link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon-180x180.png?v=eEEKWE20Q4">
		<link rel="icon" type="image/png" href="favicons/favicon-32x32.png?v=eEEKWE20Q4" sizes="32x32">
		<link rel="icon" type="image/png" href="favicons/android-chrome-192x192.png?v=eEEKWE20Q4" sizes="192x192">
		<link rel="icon" type="image/png" href="favicons/favicon-96x96.png?v=eEEKWE20Q4" sizes="96x96">
		<link rel="icon" type="image/png" href="favicons/favicon-16x16.png?v=eEEKWE20Q4" sizes="16x16">
		<link rel="manifest" href="favicons/manifest.json?v=eEEKWE20Q4">
		<link rel="mask-icon" href="favicons/safari-pinned-tab.svg?v=eEEKWE20Q4" color="#5bbad5">
		<link rel="shortcut icon" href="favicons/favicon.ico?v=eEEKWE20Q4">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="favicons/mstile-144x144.png?v=eEEKWE20Q4">
		<meta name="theme-color" content="#ffffff">
		<?php
	}

	/**
	 * Queue JS and CSS files
	 */
	public function wp_enqueue_scripts(){
		// initialize dependencies and main js localization data
		$css_deps = array('style');
		$js_deps = array('jquery');
		$js_loc = array( 'ajaxurl' => admin_url('admin-ajax.php') );

		if( class_exists('GFAPI') ){
			$forms = GFAPI::get_forms();
			$js_loc['gravityforms'] = $forms;
		}

		// make sure visual composer css is always loaded
		wp_enqueue_style('js_composer_front');

		if( is_singular() && get_option( 'thread_comments' ) ){
			wp_enqueue_script( 'comment-reply' );
		}

		if( is_page_template( 'tpl-contact.php' ) ){
			exit('change key for google maps api call');
			$this->enqueue_script( 'googleAPI', 'https://maps.googleapis.com/maps/api/js?key=[CHANGEME]&amp;sensor=false' );
			$this->enqueue_script( 'googlemap', 'googlemap.js', array('googleAPI') );
			$js_deps[] = 'googlemap';
		}

		/**
		 * @todo SHOULD DELETE THIS IF NOT USED
		 */
		$this->enqueue_style( 'google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:400,700' );

		$this->enqueue_style( 'main-css', 'style.css' );
		$this->enqueue_script( 'main-script', 'script.js', $js_deps );
		wp_localize_script( 'main-script', 'NAJ', $js_loc );

		if( is_singular() ){
			$this->enqueue_script( 'share', 'jssocials.min.js' );
			$js_deps[] = 'share';
		}

		$this->enqueue_style( 'ie-css', 'ie.css' );
		wp_style_add_data( 'ie-css', 'conditional', 'IE' );

		$this->enqueue_script( 'imagel', 'imagesloaded.pkgd.min.js' );
		$js_deps[] = 'imagel';

		$this->enqueue_script( 'isotope', 'isotope.pkgd.min.js' );
		$js_deps[] = 'isotope';

		$this->enqueue_script( 'packery', 'packery-mode.pkgd.min.js' );
		$js_deps[] = 'packery';

		$this->enqueue_script( 'inview', 'jquery.inview.js' );
		$js_deps[] = 'inview';
	}

	/**
	 * Wrapper to enqueue typekit font
	 * @param  string $key the alphanumeric key for the js file
	 */
	public function enqueue_typekit( $key ){
		add_action( 'wp_head', array($this, 'theme_typekit_inline') );
		wp_enqueue_script( "theme_typekit", "//use.typekit.net/{$key}.js", array(), NAJ_THEME_VERSION, FALSE );
	}

	/**
	 * Start the enqueued typekit file
	 */
	public function theme_typekit_inline(){
		if( wp_script_is('theme_typekit', 'done') ){
			?><script type="text/javascript">try{Typekit.load();}catch(e){}</script><?php
		}
	}

	/**
	 * Enqueue a JS file
	 *
	 * @param  string  $handle    a unique identifier for the script
	 * @param  string  $file      url for the file
	 * @param  array   $deps      unique identifiers for scripts to load before this one
	 * @param  string  $ver       version of the file
	 * @param  boolean $in_footer do we load this file in the footer
	 *
	 * @uses wp_enqueue_script()
	 */
	public function enqueue_script( $handle, $file, $deps = array(), $ver = '', $in_footer = TRUE ){
		// if no version is used, use the last modified time of the file
		if( ! $ver ){
			$ver = @filemtime( $this->jsPATH($file) );
		}

		$src = $this->jsURL($file);
		if( ! file_exists($this->jsPATH( $file )) ){
			$src = $file;
		}

		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
	}

	/**
	 * Enqueue a CSS file
	 *
	 * @param  string $handle a unique identifier for the script
	 * @param  string $file   url for the file
	 * @param  array  $deps   unique identifiers for css files to load before this one
	 * @param  string $ver    version of the file
	 * @param  string $media  media attribute for the css file
	 *
	 * @uses wp_enqueue_style()
	 */
	public function enqueue_style( $handle, $file, $deps = array(), $ver = '', $media = 'all' ){
		// if no version is used, use the last modified time of the file
		if( ! $ver ){
			$ver = @filemtime( $this->cssPATH($file) );
		}

		$src = $this->cssURL($file);
		if( ! file_exists($this->cssPATH( $file )) ){
			$src = $file;
		}

		wp_enqueue_style( $handle, $src, $deps, $ver, $media );
	}

	/**
	 * Get the url for an image file
	 *
	 * @param  string $file file name for which to get the url
	 *
	 * @return string       url for the image
	 */
	public function imgURL( $file ){
		return get_stylesheet_directory_uri() . "/images/{$file}";
	}

	/**
	 * Get the url for a JS file
	 *
	 * @param  string $file file name for which to get the url
	 *
	 * @return string       url for the js file
	 */
	public function jsURL( $file ){
		return get_stylesheet_directory_uri() . "/js/{$file}";
	}
	/**
	 * Get the path for a JS file
	 *
	 * @param  string $file file name for which to get the path
	 *
	 * @return string       path for the js file
	 *
	 */
	public function jsPATH( $file ){
		return get_stylesheet_directory() . "/js/{$file}";
	}

	/**
	 * Get the url for a CSS file
	 *
	 * @param  string $file file name for which to get the url
	 *
	 * @return string       url for the css file
	 */
	public function cssURL( $file ){
		return get_stylesheet_directory_uri() . "/css/{$file}";
	}
	/**
	 * Get the path for a CSS file
	 *
	 * @param  string $file file name for which to get the path
	 *
	 * @return string       path for the css file
	 */
	public function cssPATH( $file ){
		return get_stylesheet_directory() . "/css/{$file}";
	}

	/**
	 * Checks if current host is PAR Design DEV server
	 *
	 * @uses site_url()
	 *
	 * @return boolean is this the dev server?
	 */

	/**
	 * Writes the robots.txt file on the lab
	 *
	 * @see init()
	 * @see is_lab()
	 */
	public function block_robots(){
		$robots_path = ABSPATH . 'robots.txt';
		$robots_content = "User-agent: *\nDisallow: /";

		if( ! file_exists($robots_path) ){
			$fh = fopen( $robots_path, 'w' );

			if( $fh ){
				fwrite( $fh, $robots_content );
			}
		}
	}

	/**
	 * Add some classes to the post wrapper
	 *
	 * @param  string $classes original classes for the post wrapper
	 *
	 * @return string          modified classes for the post wrapper
	 */
	public function post_class( $classes ){
		$classes[] = 'cf'; // add float clearing class
		return $classes;
	}

	/**
	 * Make sure filenames are clean by removing accents
	 *
	 * @param  string $filename original filename
	 *
	 * @return string           sanitized filename
	 */
	public function sanitize_filename_on_upload( $filename ){
		return remove_accents( $filename );
	}

	/**
	 * Output theme pagination for archives
	 */
	public function archive_pagination(){
		global $wp_query, $wp_rewrite;

		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

		$pagination = array(
			'base' => @add_query_arg( 'page','%#%' ),
			'format' => '',
			'total' => $wp_query->max_num_pages,
			'current' => $current,
					'show_all' => false,
					'end_size'     => 1,
					'mid_size'     => 2,
			'type' => 'list',
			'next_text' => '&gt;',
			'prev_text' => '&lt;'
		);

		if( $wp_rewrite->using_permalinks() )
			$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

		if( !empty($wp_query->query_vars['s']) )
			$pagination['add_args'] = array( 's' => get_query_var( 's' ) );

		echo paginate_links( $pagination );
	}

	/**
	 * Add taxonomy classes to body tag
	 *
	 * @param  string $classes original classes
	 *
	 * @return string          modified classes
	 */
	public function custom_taxonomy_in_body_class( $classes ){
		if( is_singular('product') or is_tax('category_product') ){
			$custom_terms = get_the_terms(0, 'category_product');

			if( $custom_terms ){
				foreach( $custom_terms as $custom_term ){
					if( $custom_term->parent ){
						$custom_term = get_term( $custom_term->parent, 'category_product' );
					}

					$classes[] = 'term-' . $custom_term->slug;
				}
			}
		}

		return $classes;
	}

	/**
	 * Custom tags for gravity forms
	 *
	 * @param  array	$merge_tags		original list of tags
	 * @param  int		$form_id		id of the current form
	 * @param  array	$fields			current form's fields
	 * @param  int		$element_id		id of the current element
	 *
	 * @return array             		modified list of tags
	 */
	public function custom_merge_tags($merge_tags, $form_id, $fields, $element_id){
		$merge_tags[] = array('label' => 'URL du site', 'tag' => '{site_url}');
		$merge_tags[] = array('label' => 'Nom de domaine du site', 'tag' => '{domain_name}');

		return $merge_tags;
	}

	/**
	 * Replace merge tags with their content
	 * @param  string $text       	The current text in which merge tags are being replaced
	 * @param  object $form       	Current form
	 * @param  object $entry      	Current entry
	 * @param  boolean $url_encode 	Whether or not to encode any URLs found in the replaced value
	 * @param  boolean $esc_html   	Whether or not to encode HTML found in the replaced value
	 * @param  boolean $nl2br      	Whether or not to convert newlines to break tags
	 * @param  string $format    	Default "html"; determines how the value should be formatted
	 * @return string             	The text with the merge tags replaced
	 */
	public function replace_custom_merge_tags($text, $form, $entry, $url_encode, $esc_html, $nl2br, $format) {
		$site_url    = site_url();
		$parse       = parse_url($site_url);

		return str_replace( array( '{site_url}', '{domain_name}' ), array( $site_url, $parse['host'] ), $text );
	}

	/**
	 * [email]email@example.com[/email]
	 */
	public function sc_hide_email( $atts , $content = null ){
		if( ! is_email($content) ) return;
		return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
	}

	/**
	 * [custom-field name="fieldname"]
	 */
	public function sc_custom_field( $atts ){
		global $post;
		return get_post_meta( $post->ID, $atts['name'], TRUE );
	}

	/*** [google-map] ***/
	public function google_map( $atts ){
		get_template_part( 'includes/maps' );

		$gmap = "";

		$gmap .="<div id='map_canvas'></div>";

		echo $gmap;
	}

	/*** [pagebanner] ***/
	public function pagebanner( $atts ){
		get_template_part('/includes/page-banner');
	}

	/*** [homeblocs] ***/
	public function homeblocs( $atts ){
		get_template_part('/includes/homeblocs');
	}

	/*** [newsletter] ***/
	public function newsletter( $atts ){
		get_template_part('/includes/newsletter');
	}

	/*** [video-grid] ***/
	public function videogrid( $atts ){
		get_template_part('/includes/video-grid');
	}

	/*** [video-grid] ***/
	public function gridevents( $atts ){
		get_template_part('/includes/grid-events');
	}

	/*** [pubs=""] ****/
	public function sc_pubs( $atts ){
		ob_start();

		$a = shortcode_atts( array( 'publicite' => '', ), $atts ); ?>
		<?php $hide = ""; $link = ""; $target = ""; ?>
		<?php if( get_field($a['publicite'], 'options') ): ?>
			<?php if( get_field($a['publicite'].'_hide', 'options') == TRUE ): ?>
				<?php $hide = "mobile-hide"; ?>
			<?php endif; ?>
			<?php if( get_field('lien_'. $a['publicite'], 'options') ): ?>
				<?php $link = get_field('lien_'. $a['publicite'], 'options'); $target = "_blank";  ?>
				<a class="click-pub" href="<?php echo $link; ?>" target="<?php echo $target; ?>">
					<div class="pub-item <?php echo $hide; ?> pub-size-<?php echo $a['publicite']; ?>">
						<div class="pub-banner" style="background: url(<?php echo get_field($a['publicite'], 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
					</div>
				</a>
			<?php else: ?>
				<div class="pub-item <?php echo $hide; ?> pub-size-<?php echo $a['publicite']; ?>">
					<div class="pub-banner" style="background: url(<?php echo get_field($a['publicite'], 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php return ob_get_clean();
	}

	/*** [pagebanner] ***/
	public function single_pub( $atts ){
		ob_start();
		get_template_part('/includes/single-pub');
		return ob_get_clean();
	}



	/**
	 * [ptb-footer-lock]
	 */
	public function sc_footer_lock( $atts ){
		ob_start();
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="socials">
					<a href="https://www.facebook.com/BlondStoryBC" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/VGoudreault" target="_blank"><i class="fa fa-twitter"></i></a>
					<a href="https://instagram.com/vgoudreault/" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="https://www.youtube.com/user/blondstory" target="_blank"><i class="fa fa-youtube-play"></i></a>
					<a href="mailto:info@blondstory.com"><i class="fa fa-envelope"></i></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 years">
					© <?php echo date('Y'); ?> Blond Story
				</div>
				<div class="col-md-6 col-sm-12 naj">
					Conception web par <a class="najomie" href="http://www.najomie.com" target="_blank">najomie.com</a>
				</div>
			</div>
		</div>
		<?php
		return ob_get_clean();
	}
}

$GLOBALS['naj_functions'] = new NAJ_ThemeFunctions();

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_filter( 'woocommerce_checkout_fields' , 'blondstory_override_checkout_fields' );

function blondstory_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_company']);
	 unset($fields['shipping']['shipping_company']);

     return $fields;
}

add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_phone' );

function custom_override_billing_phone( $billing_fields ) {
     $billing_fields['billing_phone']['required'] = false;

     return $billing_fields;
}

add_filter( 'gform_submit_button_1', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button' id='gform_submit_button_{$form['id']}'><i class='fa fa-chevron-right'></i></button>";
}

function get_first_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all( '/<img .+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches );
    $first_img = $matches[1][0];
    if ( empty( $first_img ) ) {

        $first_img = get_template_directory_uri() . "/images/default.jpg";
    }
    return $first_img;
}


/* ------------------- Custom Pagination */

function pagination($pages = '', $range = 2) {
		 $showitems = ($range * 2)+1;

		 global $paged;
		 if(empty($paged)) $paged = 1;

		 if($pages == '') {
			 global $wp_query;
			 $pages = $wp_query->max_num_pages;
			 if(!$pages) {
				 $pages = 1;
			 }
		 }

		 if(1 != $pages) {
			 echo "<div class='pagination pagination-centered'><ul>";
			 if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo;</a><li>";
			 if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";

			 for ($i=1; $i <= $pages; $i++) {
				 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
					 echo "<li>";
					 echo ($paged == $i)? "<a class='active' href='#'>".$i."</a>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
					 echo "</li>";
				 }
			 }

			 if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>";
			 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
			 echo "</ul></div>\n";
		 }
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Publicités',
		'menu_title'	=> 'Publicités',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'icon_url' => 'dashicons-megaphone',
		'redirect'		=> false
	));
}

function blond_excerpt($charlength) {
	$excerpt = get_the_excerpt();
	$excerpt = str_replace("&nbsp;","",$excerpt);
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 3 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '<span class="dots">...</span>';
	} else {
		echo $excerpt;
	}
}

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );

/**
 * Set maximum shipping cost in WooCommerce
 */

 /*add_filter( 'woocommerce_package_rates' , 'woocommerce_set_maximum_shipping_cost', 10, 2 );
 function woocommerce_set_maximum_shipping_cost( $rates, $package ) {

 	foreach( $rates as $rate ) {
 		if( $rate->cost > 13 ) {
 			$rate->cost = 13;
 			$rate->taxes[13] = (13 * 0.05);
 			$rate->taxes[14] = (13 * 0.09975);
 		}

 	}

 	return $rates;

 } */

add_action( 'template_redirect', 'redirect_to_specific_page' );

function redirect_to_specific_page() {

if ( ! current_user_can('manage_woocommerce') && is_woocommerce() ) {

wp_redirect( home_url() );
  exit;
    }
}
