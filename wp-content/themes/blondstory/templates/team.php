<?php /* Template name: Équipe  */ ?>
<?php get_header(); ?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<section class="content container">

    <?php if ( have_rows('membres') ) : ?>
        <div class="members row">
            <h1><?php _e( 'Rencontrez l&rsquo;équipe')?></h1>
            <div class="col-sm-12">
                <hr class="sep after-title">
            </div>
            <?php while ( have_rows('membres') ) : the_row(); $departement = get_sub_field('nom_departement'); ?>

                <?php $classes = ""; if( $departement == "associe" ): $classes = "col-xs-6"; else: $classes = "col-xs-4"; endif; ?>

                <div class="<?php echo $departement; ?> inner-members col-sm-12">
                    <?php if ( have_rows('departement') ) : ?>
                        <div class="row">
                            <?php while ( have_rows('departement') ) : the_row(); ?>
                                <?php
                                    $image   = get_sub_field('photo_du_membre');
                                    $nom   = get_sub_field('nom');
                                    $titre    = get_sub_field('titre');
                                 ?>
                                 <div class="<?php echo $classes; ?> single-membre">
                                     <img src="<?php echo $image; ?> " alt="" />
                                     <h3><?php echo $nom; ?></h3>
                                     <h4><?php echo $titre; ?></h4>
                                 </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-12">
                    <hr class="sep">
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

</section>
<?php wp_reset_query(); ?>
<?php endwhile; endif; ?>

<?php get_footer();?>
