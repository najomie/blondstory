jQuery(document).ready(function($) {

	$('#wpadminbar').addClass('Fixed');
	$('.mobile-hide').parents().eq(2).addClass('top-mobile-hide');

	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$('.mobile-menu').slideToggle(300);
		return false;
	});

	// SEARCH
	$('a[href="#search"]').on('click', function(event) {
		event.preventDefault();
		$('.md-overlay').addClass('down');
		$( "#search" ).slideDown( 400, function() {});
		$('.searchform input[type="search"]').focus();
	});

	$('a[href="#close"]').on('click', function(event) {
		$( "#search" ).slideUp( 400, function() {});
		$('.md-overlay').removeClass('down');;
	});

	if ( $.fn.jsSocials ) {
		bs_socials();
	}

	// BACK TO TOP
	var offset = 300,
	offset_opacity = 1,
	scroll_top_duration = 700,
	$back_to_top = $('.cd-top');


	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() < offset_opacity ) {
			$back_to_top.addClass('cd-fade-out');
		}

		if ($(window).width() > 1024) {
			if( $('.archive .pub-item').length || $('.single-collaborateur .pub-item').length ){
				var element_position = $('.sidebar').offset().top;
				var inner_height = $('.sidebar .inner').height();
				var v_position = $('.instagram-section').offset().top;
				var y_scroll_pos = window.pageYOffset;

				if ( ( y_scroll_pos - 60 ) >= element_position ){
					$('.sidebar').addClass('fixed');
				}
				if ( ( y_scroll_pos - 60 ) < element_position || ( y_scroll_pos + inner_height + 60 ) > v_position ){
					$('.sidebar').removeClass('fixed');
				}
			}

			if( $('.single-post .pub-item').length ){
				var element_position = $('.sidebar').offset().top;
				var inner_height = $('.sidebar .inner').height();
				var v_position = $('.similar-posts').offset().top;
				var y_scroll_pos = window.pageYOffset;

				if ( ( y_scroll_pos - 60 ) >= element_position ){
					$('.sidebar').addClass('fixed');
				}
				if ( ( y_scroll_pos - 60 ) < element_position || ( y_scroll_pos + inner_height + 30 ) > v_position ){
					$('.sidebar').removeClass('fixed');
				}
			}

			if( $('.home .pub-side .pub-item').length ){
				var element_position = $('.sidebar').offset().top;
				var inner_height = $('.sidebar .inner').height();
				var v_position = $('#video-grid').offset().top;
				var y_scroll_pos = window.pageYOffset;

				if ( ( y_scroll_pos - 60 ) >= element_position ){
					$('.sidebar').addClass('fixed');
				}
				if ( ( y_scroll_pos - 60 ) < element_position || ( y_scroll_pos + inner_height + 30 ) > v_position ){
					$('.sidebar').removeClass('fixed');
				}
			}
		}
	});

	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

	// MAGAZINE
	var $container = $('.archive-list');
	var $post_grid = $('.post-in-grid');
	var $events = $('.past-events');

	if( $post_grid.length ){
		$('.post-in-grid').bind('inview', function (event, visible, topOrBottomOrBoth) {
		  if (visible == true) $(this).find('.post-thumb').addClass('inview');
		});
	}

	$('.sub-toggle').click(function(){
		$('.sub-cat-toggle').toggleClass('sub-active');
		$('.top-cat-menu').slideToggle(300);
		return false;
	});

	if ( $events.length){
		$events.bind('inview', function (event, visible, topOrBottomOrBoth) {
		  if (visible == true) $(this).find('.post-thumb').addClass('inview');
		});
	}

});


function bs_socials(){

	$ = jQuery;

	$(".share-list").each(function() {
		$(this).jsSocials({
			shareIn: "popup",
			text : $(this).data('title'),
			url : $(this).data('url'),
			showCount: false,
			showLabel: false,
			shares: [
				{ share: "facebook" },
				{ share: "twitter" },
				{ share: "pinterest" },
				{ share: "email", logo: "fa fa-paper-plane" }
			]
		});
	});

	$( ".share-list .jssocials-shares" ).append( "<div class='comment-anchor'><a href='#fcomments'><i class='fa fa-comment-o'></i></a></div" );
}
