var locations = [
  ['<div style="color:#333;line-height:1.35;overflow:hidden;white-space:nowrap;font-size:15px;"><h4><strong style="color:#333">PAR Design</strong></h4>602 Curé-Boivin, Boisbriand<br><a target="_blank" href="#">Voir sur Google Map</a></div>', 45.622082, -73.829093],
];

var map = new google.maps.Map(document.getElementById('googleMap'), {
	zoom: 16,
	center: new google.maps.LatLng(45.622082, -73.829093),
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	scrollwheel: false,
	draggable: true,
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {  
	marker = new google.maps.Marker({
		position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		map: map,
		animation: google.maps.Animation.DROP,
	});

	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
			infowindow.setContent(locations[i][0]);
			infowindow.open(map, marker);
		}
	})(marker, i));

}