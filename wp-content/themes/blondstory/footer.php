<?php
global $naj_functions;
$json_link ="https://www.instagram.com/vgoudreault/media/";
$json      = file_get_contents($json_link);
$obj 	   = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
?>
		</div>

		<div class="instagram-section">
			<h2><?php _e('Suivez Blond Story sur instagram'); ?></h2>
			<?php if( $obj ): $count = 0; ?>
				<div class="insta-grid">
					<?php foreach ($obj['items'] as $feed): $count++; ?>
						<?php
							$link = $feed['link'];
							$fullsize = $feed['images']['standard_resolution']['url'];
						?>
						<a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $fullsize; ?>" alt=""></a>
					<?php if( $count == 8 ): break; endif; endforeach; ?>
				</div>
			<?php endif; ?>
		</div>

		<footer class="footer news-contact container">
			<div class="newsletter-footer">
				<a class="logo" href="<?php echo site_url() ?>">
					<img src="<?php echo $naj_functions->imgURL('logo.jpg'); ?>" alt="<?php bloginfo('name') ?>" />
				</a>
				<h4><?php _e('Recevez l&rsquo;infolettre mensuelle'); ?></h4>
				<?php echo do_shortcode( '[newsletter]' ); ?>
			</div>
			<div class="inner-footer"></div>
			<div class="credits">
				<?php echo do_shortcode( '[ptb-footer-lock]' ); ?>
			</div>
		</footer>


	</div>
	<?php wp_footer(); ?>
	<a href="#0" class="cd-top">Top</a>
</body>
</html>
