<?php global $naj_functions;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html id="top" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />

	<title><?php wp_title(); ?></title>

	<div id="fb-root"></div>
	<?php /*
	<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/fr_CA/sdk.js#xfbml=1&version=v2.5&appId=390217967676311";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> */ ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="search-wrap">
		<div id="search">
			<div class="container-fluid">
					<form class="searchform" method="get" action="<?php echo site_url(); ?>">
						<div class="searchpop">
							<input id="s" type="search" value="" name="s" placeholder="<?php _e( 'Recherche...');?>" />
						</div>
						<a href="#close" class="s-close"><i class="fa fa-close"></i></a>
					</form>
			</div>
		</div>
	</div>

	<div class="page-wrapper">

		<header class="header">
			<div class="container">
				<div class="pull-left">
					<?php wp_nav_menu( array( 'theme_location' => 'top-nav', 'container' => 'nav', 'container_class' => '', 'fallback_cb' => 'false' )); ?>
				</div>
				<div class="pull-right">
					<a href="#search" class="search toggle-search"><i class="fa fa-search"></i></a>
					<a class="cart-but" href="<?php echo WC()->cart->get_cart_url(); ?>"><span>Mon panier</span><i class="fa fa-shopping-cart"></i></a>
				</div>
				<div class="col-sm-12 logo-wrapper">
					<a class="logo" href="<?php echo site_url() ?>">
						<img src="<?php echo $naj_functions->imgURL('logo.jpg'); ?>" alt="<?php bloginfo('name') ?>" />
					</a>
				</div>
				<div class="center-nav">
					<?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => 'nav', 'container_class' => '', 'fallback_cb' => 'false' )); ?>
				</div>
			</div>
		</header>

		<?php get_template_part('includes/mobile-header'); ?>

		<div id="main" class="main">

		<?php if( ( is_home() || is_single() || is_archive() || is_search() ) && !is_woocommerce() ): ?>
			<?php /*<div class="sub-cat-wrap">
				<div class="sub-toggle">
					<h5><?php _e( 'Menu du magazine')?></h5>
					<a class="sub-cat-toggle"><i class="fa fa-chevron-right"></i></a>
				</div>
				<aside class="small-center-nav top-cat-menu">
					<?php $terms = get_terms( 'category', array( 'parent' => 0,'hide_empty' => 0, 'exclude' => array(1,1100,44),  ) );
					echo '<ul class="magazine-nav">';
					foreach ( $terms as $term ) {
						$term_link = get_term_link( $term );
						$cat = get_query_var('cat');
						$current_cat = get_category($cat);

						if( !empty($cat) && $term->slug == $current_cat->slug || !empty($cat) && $term->term_id == $current_cat->parent ): $class = 'current tax'; else: $class = 'tax'; endif;
						echo '<li class="'.$class.'"><a class="'.$class.'" href="' . esc_url( $term_link ) . '"><span>' . $term->name . '</span></a></li>';
					}
					echo '</ul>'; ?>
				</aside>
			</div> */ ?>

			<?php if( get_field('leaderboard4', 'options') ): ?>
				<?php if( get_field('lien_leaderboard4', 'options') ): ?>
					<div class="container">
						<a class="click-pub" href="<?php echo get_field('lien_leaderboard4', 'options'); ?>" target="_blank">
							<div class="pub-item pub-size-leaderboard4">
								<div class="pub-banner" style="background: url(<?php echo get_field('leaderboard4', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
							</div>
			        	</a>
					</div>
			    <?php else: ?>
					<div class="container">
						<div class="pub-item pub-size-leaderboard4">
							<div class="pub-banner" style="background: url(<?php echo get_field('leaderboard4', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
						</div>
					</div>
			    <?php endif; ?>
			<?php endif; ?>

		<?php endif; ?>
