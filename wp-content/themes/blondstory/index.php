<?php get_header(); ?>
<section class="content container">

	<div class="row row-grid">
		<aside class="col-sm-push-8 col-sm-4 sidebar">
			<div class="inner">
				<?php get_template_part('includes/pub-sidebar'); ?>
				<?php dynamic_sidebar( 'blog-sidebar' ); ?>
			</div>
		</aside>

		<div class="post-list-third archive-list col-sm-pull-4 col-sm-8">

			<?php $count = 0; if(have_posts()) : while( have_posts() ) : the_post(); $count ++; ?>

				<?php include(locate_template('includes/list-post.php')); ?>

			<?php wp_reset_query(); endwhile; endif; ?>
		</div>
		
		<?php echo pagination(); ?>
	</div>
</section>

<?php get_footer();
