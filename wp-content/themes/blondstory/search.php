<?php get_header(); ?>
<section class="content container">

	<div class="title search-title">
		<h1><?php _e('Résultat(s) de recherche', 'inspire');?>: "<?php the_search_query(); ?>"</h1>
	</div>

	<div class="row row-grid">
		<div class="post-rows">

			<div class="row post-grid">

				<?php $count = 0; if(have_posts()) : while( have_posts() ) : the_post(); $count ++; ?>

					<?php if( $count == 2 && get_field('box1', 'options') ): ?>
						<div class="mobile-pub col-sm-6 pub-item pub-size-box">
							<div class="pub-banner" style="background: url(<?php echo get_field('box1', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
						</div>
					<?php elseif( $count == 3 && get_field('box2', 'options') ): ?>
						<div class="mobile-pub col-sm-6 pub-item pub-size-box">
					        <div class="pub-banner" style="background: url(<?php echo get_field('box2', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
					    </div>
					<?php elseif( $count == 4 && get_field('box3', 'options') ): ?>
						<div class="mobile-pub col-sm-6 pub-item pub-size-box">
							<div class="pub-banner" style="background: url(<?php echo get_field('box3', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
						</div>
					<?php elseif( $count == 5 && get_field('bigbox1', 'options') ): ?>
						<div class="mobile-pub col-sm-6 pub-item pub-size-bigbox">
							<div class="pub-banner" style="background: url(<?php echo get_field('bigbox1', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
						</div>
					<?php endif; ?>

					<?php $categories = get_the_category( $post->ID ); ?>

					<div class="post-list col-sm-6 <?php echo $count; ?>">

						<article class="post post-item <?php if( !empty($categories)): echo $categories[0]->slug; endif; ?>">

							<?php if( !empty($categories) && ( $categories[0]->slug == "videos" ) && get_field('video_facebook') != "" ): ?>
								<div class="video-wrap post-thumbnail">
									<?php the_field('video_facebook'); ?>
								</div>
							<?php elseif( has_post_thumbnail() ): ?>
								<figure class="post-thumbnail">
									<a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?></a>
								</figure>
							<?php else: ?>
								<figure class="post-thumbnail">
								<a href="<?php echo the_permalink(); ?>"><img src="<?php echo get_first_image(); ?>" alt="" /></a>
								</figure>
							<?php endif; ?>

							<div class="post-title">
								<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
								<aside class="meta">
									<?php the_time('j F Y') ?></span><span>|</span>
									<?php the_category(', '); ?></span>
								</aside>
							</div>

							<p class="excerpt">
								<?php blond_excerpt(170);?>
							</p>
							<a class="more" href="<?php echo get_permalink(); ?>"><?php _e( 'Lire la suite'); ?> <span></span></a>

						</article>

					</div>

				<?php wp_reset_query(); ?>
				<?php endwhile; endif; ?>
			</div>
			<?php echo pagination(); ?>
		</div>

		<div class="fixed-nav">
			<div class="fixed-inner">
				<aside class="sidebar">
					<?php get_template_part('includes/pub-sidebar'); ?>
					<?php dynamic_sidebar( 'blog-sidebar' ); ?>
				</aside>
			</div>
		</div>
	</div>
</section>
<?php get_footer();
