<?php get_header(); ?>

<?php
$collabs = get_field('collab_single');
$profile = get_the_author_meta( 'ID' );
$fname = get_the_author_meta( 'first_name' );
$lname = get_the_author_meta( 'last_name' );
$name  = $fname.' '.$lname;

$photo = get_avatar_url( $profile );
if($collabs){
	$name  = $collabs[0]->post_title;
	$photo = get_field('photo', $collabs[0]->ID );
}

$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID), 'orderby' => 'rand' ) );

?>
<section class="content container single-post-container">

	<div class="row">

		<aside class="col-sm-push-8 col-sm-4 sidebar">
			<div class="inner">
				<?php get_template_part('includes/pub-sidebar'); ?>
				<?php dynamic_sidebar( 'blog-sidebar' ); ?>
			</div>
		</aside>

		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<div class="post-list col-sm-pull-4 col-sm-8">

				<article class="post">

					<div class="post-title-block">
						<h1><?php the_title(); ?></h1>
					</div>

					<aside class="metas">
						<div class="author-info">
							<figure style="background-image:url(<?php echo $photo; ?>);"></figure>
							<span><?php _e('par', 'theme'); ?> <?php echo $name; ?></span>
							<span class="sep">|</span>
							<span><?php the_time('j F Y') ?></span>
							<span class="sep">|</span>
							<?php the_category(', '); ?></span>
						</div>
						<div class="share-list" data-title="<?php echo the_title(); ?>" data-url="<?php echo get_permalink(); ?>">
						</div>
					</aside>

					<?php if ( has_post_thumbnail() ) : ?>
					<figure class="post-thumbnail">
						<a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?></a>
					</figure>
					<?php endif; ?>

					<div class="single-post-content">
						<?php the_content(); ?>
					</div>

					<?php

					$collabs = get_field('collab_single');

					if( $collabs ): ?>
					    <?php foreach( $collabs as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($post); ?>
							<?php get_template_part('includes/author-area'); ?>
					    <?php endforeach; ?>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>

					<div class="mobile-sidebar">
						<?php get_template_part('includes/pub-sidebar'); ?>
					</div>

					<div id="fcomments">
						<?php echo do_shortcode('[fbcomments]'); ?>
					</div>

				</article>

			</div>

		<?php wp_reset_query(); ?>
		<?php endwhile; endif; ?>
	</div>

</section>

<?php if( $related ): ?>
	<div class="similar-posts post-list-third in-gray">
		<div class="row">
			<div class="col-sm-12 title-suggested">
				<h2><?php _e('Articles suggérés') ?></h2>
			</div>
			<?php foreach( $related as $post ):

				setup_postdata($post);

				$thumb = get_the_post_thumbnail_url();

				if( $thumb == NULL ){
					$thumb = get_first_image();
				}
			?>
				<div class="col-sm-4">
					<a href="<?php echo get_permalink(); ?>" class="post-in-grid">
						<figure class="post-thumb">
							<div class="inner" style="background-image:url(<?php echo $thumb; ?>);"></div>
						</figure>
						<h3><?php the_title() ?></h3>
					</a>
				</div>
			<?php endforeach;  wp_reset_postdata(); ?>
		</div>
	</div>
<?php endif; ?>

<?php get_footer();
