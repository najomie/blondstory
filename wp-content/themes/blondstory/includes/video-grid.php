<div class="post-list latest-video">
    <?php

        $args = array(
            'cat' => 20,
            "orderby" => "date",
            'post_status' => 'publish',
            'posts_per_page' => 2,
            "order"   => "DESC"
        );
        $wp_query = new WP_Query($args);

    ?>
    <?php if ($wp_query->have_posts()): $count = 0; while ($wp_query->have_posts()) : $wp_query->the_post(); $count ++; ?>
        <div class="col-md-6 col-sm-6 post-item video">
            <?php if( get_field('video_facebook') ): ?>
                <div class="video-wrap">
                    <?php the_field('video_facebook'); ?>
                </div>
            <?php endif; ?>
            <div class="post-title">
                <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
            </div>
            <p class="excerpt"><?php blond_excerpt(170);?></p>
            <a class="more" href="<?php echo get_permalink(); ?>">Voir la vidéo <span></span></a>
        </div>
    <?php endwhile; endif; wp_reset_query(); ?>
    <div class="col-sm-12 more-video">
        <?php $category_link = get_category_link( 20 ); ?>
        <a class="more-cta" href="<?php echo $category_link; ?>"><?php _e( 'Voir toutes les vidéos'); ?></a>
    </div>
</div>
