<?php global $naj_functions; ?>
<div class="post-list past-events">
    <?php

        $args = array(
            'cat' => 1100,
            "orderby" => "date",
            'post_status' => 'publish',
            'posts_per_page' => 12,
            "order"   => "DESC"
        );
        $wp_query = new WP_Query($args);

    ?>
    <h2><?php _e( 'Événements passés')?></h2>
    <?php if ($wp_query->have_posts()): $count = 0; while ($wp_query->have_posts()) : $wp_query->the_post(); $count ++; ?>
        <div class="col-md-6 col-sm-6 post-item event">
            <?php if( has_post_thumbnail() ): ?>
                <figure class="post-thumbnail">
                    <a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?></a>
                </figure>
            <?php else: ?>
                <figure class="post-thumbnail">
                    <a href="<?php echo the_permalink(); ?>">
                        <div class="events-thumb" style="background: url(<?php echo get_first_image(); ?>); background-size: cover; background-repeat: no-repeat; background-position: 50% 87%;"></div>
                    </a>
                </figure>
            <?php endif; ?>
            <h3 class="post-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p>
                <?php blond_excerpt(170);?>
            </p>
            <a class="more" href="<?php echo get_permalink(); ?>"><?php _e( 'En lire plus')?> <span></span></a>
        </div>
    <?php endwhile; endif; wp_reset_query(); ?>
</div>
