<?php if( get_field('bigbox1', 'options') ): ?>
    <a class="click-pub" <?php if( get_field('lien_bigbox1', 'options') ): ?>href="<?php echo get_field('lien_bigbox1', 'options'); ?>" target="_blank"<?php endif; ?>>
        <div class="pub-item pub-size-bigbox">
            <div class="pub-banner">
                <img src="<?php echo get_field('bigbox1', 'options'); ?>" alt="Blond Story">
            </div>
        </div>
    </a>
<?php endif; ?>

<?php if( get_field('box1', 'options') ): ?>
    <a class="click-pub" <?php if( get_field('lien_box1', 'options') ): ?>href="<?php echo get_field('lien_box1', 'options'); ?>" target="_blank" <?php endif; ?>>
        <div class="pub-item pub-size-box">
            <div class="pub-banner">
                <img src="<?php echo get_field('lien_box1', 'options'); ?>" alt="Blond Story">
            </div>
        </div>
    </a>
<?php endif; ?>

<?php if( get_field('box2', 'options') ): ?>
    <a class="click-pub" <?php if( get_field('lien_box2', 'options') ): ?>href="<?php echo get_field('lien_box2', 'options'); ?>" target="_blank" <?php endif; ?>>
        <div class="pub-item pub-size-box">
            <div class="pub-banner">
                <img src="<?php echo get_field('lien_box2', 'options'); ?>" alt="Blond Story">
            </div>
        </div>
    </a>
<?php endif; ?>

<?php if( get_field('box3', 'options') ): ?>
    <a class="click-pub" <?php if( get_field('lien_box3', 'options') ): ?>href="<?php echo get_field('lien_box3', 'options'); ?>" target="_blank" <?php endif; ?>>
        <div class="pub-item pub-size-box">
            <div class="pub-banner">
                <img src="<?php echo get_field('lien_box3', 'options'); ?>" alt="Blond Story">
            </div>
        </div>
    </a>
<?php endif; ?>
