<?php global $naj_functions; ?>
<header class="mobile-header">
    <div class="container">
        <div class="top-mobile">
            <a class="logo" href="<?php echo site_url() ?>"><img src="<?php echo $naj_functions->imgURL('logo.jpg'); ?>" alt="<?php bloginfo('name') ?>" /></a>
            <button class="c-hamburger hamburger c-hamburger--htx">
			  <span>toggle menu</span>
			</button>
        </div>
        <div class="mobile-menu">
            <ul class="top-m">
                <?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => false, 'items_wrap' => '%3$s' )); ?>
                <?php wp_nav_menu( array( 'theme_location' => 'top-nav', 'container' => false, 'items_wrap' => '%3$s' )); ?>
            </ul>
            <ul class="bot-m">

               <form class="searchform" method="get" action="<?php echo site_url(); ?>">
						<div class="searchpop">
							<input id="s" type="search" value="" name="s" placeholder="<?php _e( 'Recherche...');?>" />
						</div>
					</form>

                <li class="soc">
                    <a href="https://www.facebook.com/BlondStoryBC" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="soc">
                    <a href="https://twitter.com/VGoudreault" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="soc">
                    <a href="https://instagram.com/vgoudreault/" target="_blank"><i class="fa fa-instagram"></i></a>
                </li>
                <li class="soc">
                    <a href="https://www.youtube.com/user/blondstory" target="_blank"><i class="fa fa-youtube-play"></i></a>
                </li>
            </ul>
        </div>
    </div>
</header>
