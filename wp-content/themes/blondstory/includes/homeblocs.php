<?php if ( have_rows('bloc') ) : ?>
    <div class="homeblocs">
        <?php while ( have_rows('bloc') ) : the_row(); ?>
            <?php
                $image   = get_sub_field('image');
                $texte   = get_sub_field('texte');
                $lien    = get_sub_field('lien');
             ?>
            <a href="<?php echo $lien; ?>" class="bloc col-sm-4">
                <figure style="background: url(<?php echo $image; ?>) 50% 50% / cover no-repeat;">
                    <figcaption><h2><?php echo $texte; ?></h2></figcaption>
                </figure>
            </a>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
