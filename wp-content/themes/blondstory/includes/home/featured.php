<?php $featured_posts = get_field('featured_posts'); ?>

<?php if( $featured_posts ): ?>
    <div class="first-featured post-list-third">
        <div class="row">
            <?php foreach( $featured_posts as $post ):

                setup_postdata($post);

                $thumb = get_the_post_thumbnail_url();

                if( $thumb == NULL ){
                    $thumb = get_first_image();
                }
            ?>
                <div class="col-sm-4">
                    <a href="<?php echo get_permalink(); ?>" class="post-in-grid">
                        <figure class="post-thumb">
                            <div class="inner" style="background-image:url(<?php echo $thumb; ?>);"></div>
                        </figure>
                        <h3><?php the_title() ?></h3>
                    </a>
                </div>
            <?php endforeach;  wp_reset_postdata(); ?>
        </div>
    </div>
<?php endif; ?>
