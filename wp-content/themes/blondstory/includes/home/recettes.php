<?php

    $args = array(
        'cat' => 25,
        "orderby" => "date",
        'post_status' => 'publish',
        'posts_per_page' => 3,
        "order"   => "DESC"
    );

    $videos = get_posts($args);
    $category_link = get_category_link( 25 );

?>
<?php if( $videos ): ?>
	<div class="similar-posts post-list-third">
		<div class="row">
			<div class="col-sm-12 title-suggested">
				<h2><span><?php _e('Recettes') ?></span> <a class="more-cta" href="<?php echo $category_link; ?>"><?php _e( 'Voir toutes les recettes >'); ?></a></h2>
			</div>
			<?php foreach( $videos as $post ):

				setup_postdata($post);

				$thumb = get_the_post_thumbnail_url();

				if( $thumb == NULL ){
					$thumb = get_first_image();
				}
			?>
				<div class="col-sm-4">
					<a href="<?php echo get_permalink(); ?>" class="post-in-grid">
						<figure class="post-thumb">
                            <div class="inner" style="background-image:url(<?php echo $thumb; ?>);"></div>
                        </figure>
						<h3><?php the_title() ?></h3>
					</a>
				</div>
			<?php endforeach;  wp_reset_postdata(); ?>
		</div>
	</div>
<?php endif; ?>
