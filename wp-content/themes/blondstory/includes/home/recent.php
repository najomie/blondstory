<?php

global $post;
$args = array( 'posts_per_page' => 5, 'order'=> 'DESC', 'orderby' => 'date' );
$r_posts = get_posts( $args );

?>

<?php if( $r_posts ): ?>
    <div class="featured post-list-third">
        <div class="col-sm-8 posts-list">
            <div class="title-suggested">
                <h2><?php _e('Articles suggérés') ?></h2>
            </div>
            <?php foreach( $r_posts as $post ):

                setup_postdata($post);

                $thumb = get_the_post_thumbnail_url();
                $collabs = get_field('collab_single');
                $profile = get_the_author_meta( 'ID' );
                $fname = get_the_author_meta( 'first_name' );
                $lname = get_the_author_meta( 'last_name' );
                $name  = $fname.' '.$lname;

                $photo = get_avatar_url( $profile );
                $author_link = get_permalink();

                if($collabs){
                    $name  = $collabs[0]->post_title;
                    $photo = get_field('photo', $collabs[0]->ID );
                    $author_link = get_permalink($collabs[0]->ID);
                }

                if( $thumb == NULL ){
                    $thumb = get_first_image();
                }
            ?>
                <div class="post-in-grid with-desc">
                    <a class="img-block" href="<?php echo get_permalink(); ?>">
                        <figure class="post-thumb">
                            <div class="inner" style="background-image:url(<?php echo $thumb; ?>);"></div>
                        </figure>
                    </a>
                    <div class="meta-block">
                        <a href="<?php echo get_permalink(); ?>">
                            <h3><?php the_title() ?></h3>
                            <span class="date-meta"><?php the_time('j F Y') ?></span>
                            <p class="excerpt">
                                <?php blond_excerpt(155);?>
                            </p>
                        </a>
                        <aside class="metas">
                            <a class="author-info" href="<?php echo $author_link; ?>">
                                <figure style="background-image:url(<?php echo $photo; ?>);"></figure>
                                <span><?php _e('par', 'theme'); ?> <?php echo $name; ?></span>
                            </a>
                        </aside>
                    </div>
                </div>
            <?php endforeach;  wp_reset_postdata(); ?>
        </div>
        <div class="col-sm-4 pub-side sidebar">
            <div class="inner">
                <?php get_template_part('includes/pub-sidebar'); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
