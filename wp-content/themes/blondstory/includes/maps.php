<?php
   /*
   * Given an address, return the longitude and latitude using The Google Geocoding API V3
   *
   */
   $gmap_color = '#96c2bf';
   $adresse = '5056 chemin de la Côte-des-Neiges';
   $ville = 'Montréal';
   $google_geo = $adresse .', '.$ville;


?>
<script>

    function loadScript() {
       var script = document.createElement('script');
       script.type = 'text/javascript';
       script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
           '&callback=initialize';
       document.body.appendChild(script);
     }

     window.onload = loadScript;

   var geocoder;
   var map;
   var MY_MAPTYPE_ID = 'custom_style';
   function initialize() {
       geocoder = new google.maps.Geocoder();
       var latlng = new google.maps.LatLng(codeAddress());
       var isDraggable = jQuery(document).width() > 780 ? true : false;
       var mapOptions = {
           zoom: 13,
           scrollwheel: false,
           draggable: isDraggable,
           center: latlng,
           disableDefaultUI: true,
           mapTypeControlOptions: {
             mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
           },
           mapTypeId: MY_MAPTYPE_ID
       }
       map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

         var featureOpts = [

             {
           "featureType": "landscape",
           "stylers": [
               {
                   "saturation": -100
               },
               {
                   "lightness": 65
               },
               {
                   "visibility": "on"
               }
           ]
       },
       {
           "featureType": "poi",
           "stylers": [
               {
                   "saturation": -100
               },
               {
                   "lightness": 51
               },
               {
                   "visibility": "simplified"
               }
           ]
       },
       {
           "featureType": "road.highway",
           "stylers": [
               {
                   "saturation": -100
               },
               {
                   "visibility": "simplified"
               }
           ]
       },
       {
           "featureType": "road.arterial",
           "stylers": [
               {
                   "saturation": -100
               },
               {
                   "lightness": 30
               },
               {
                   "visibility": "on"
               }
           ]
       },
       {
           "featureType": "road.local",
           "stylers": [
               {
                   "saturation": -100
               },
               {
                   "lightness": 40
               },
               {
                   "visibility": "on"
               }
           ]
       },
       {
           "featureType": "transit",
           "stylers": [
               {
                   "saturation": -100
               },
               {
                   "visibility": "simplified"
               }
           ]
       },
       {
           "featureType": "administrative.province",
           "stylers": [
               {
                   "visibility": "off"
               }
           ]
       },
       {
           "featureType": "water",
           "elementType": "labels",
           "stylers": [
               {
                   "visibility": "on"
               },
               {
                   "lightness": -25
               },
               {
                   "saturation": -100
               }
           ]
       },
       {
           "featureType": "water",
           "elementType": "geometry",
           "stylers": [
               {
                   "hue": "#ffff00"
               },
               {
                   "lightness": -25
               },
               {
                   "saturation": -97
               }
           ]
       }
      ];

       var styledMapOptions = {
           name: 'Custom Style'
         };

         var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

         map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
   }


   function codeAddress() {
       var address = "<?php echo $google_geo; ?>";
       var marker = '';
       var iconBase = '<?php bloginfo('template_url'); ?>/images/marker.png';
       geocoder.geocode({'address': address}, function(results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
               map.setCenter(results[0].geometry.location);
               marker = new google.maps.Marker({
                   map: map,
                   icon: iconBase,
                   position: results[0].geometry.location,
                   url: 'https://goo.gl/maps/XF69i/'
               });
           } else {
               console.log("Geocode was not successful for the following reason: " + status);
           }
       });
       return marker;

       google.maps.event.addListener(marker, 'click', function() {
            window.open(marker.url);
        });
   }

</script>
