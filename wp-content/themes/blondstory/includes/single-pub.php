<?php if( get_field('single_pub') ): ?>
     <a class="click-pub single-pub desktop" href="<?php echo get_field('single_pub_link'); ?>" target="_blank">
         <img src="<?php echo get_field('single_pub'); ?>" alt="" />
     </a>
<?php endif; ?>

<?php if( get_field('single_mobile_pub') ): ?>
     <a class="click-pub single-pub mobile" href="<?php echo get_field('single_mobile_pub_link'); ?>" target="_blank">
         <img src="<?php echo get_field('single_mobile_pub'); ?>" alt="" />
     </a>
<?php endif; ?>
