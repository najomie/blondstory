<?php if( get_field('page_banner') ): ?>
    <div class="page-banner" style="background: url(<?php echo get_field('page_banner'); ?>); background-size: cover; background-repeat: no-repeat;">
        <?php if( get_field('titre_banner') ): ?>
            <h1><?php echo get_field('titre_banner'); ?></h1>
        <?php else: ?>
            <h1><?php the_title(); ?></h1>
        <?php endif; ?>
    </div>
<?php endif; ?>
