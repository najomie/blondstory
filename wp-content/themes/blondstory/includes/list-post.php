<?php
$thumb = get_the_post_thumbnail_url();
$collabs = get_field('collab_single');
$profile = get_the_author_meta( 'ID' );
$fname = get_the_author_meta( 'first_name' );
$lname = get_the_author_meta( 'last_name' );
$name  = $fname.' '.$lname;

$photo = get_avatar_url( $profile );

$author_link = get_permalink();

if($collabs){
    $name  = $collabs[0]->post_title;
    $photo = get_field('photo', $collabs[0]->ID );
    $author_link = get_permalink($collabs[0]->ID);
}

if( $thumb == NULL ){
    $thumb = get_first_image();
}
?>
<?php if( $count == 2 && get_field('box1', 'options') ): ?>
    <div class="mobile-pub col-sm-6 pub-item pub-size-box">
        <div class="pub-banner" style="background: url(<?php echo get_field('box1', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
    </div>
<?php elseif( $count == 3 && get_field('box2', 'options') ): ?>
    <div class="mobile-pub col-sm-6 pub-item pub-size-box">
        <div class="pub-banner" style="background: url(<?php echo get_field('box2', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
    </div>
<?php elseif( $count == 4 && get_field('box3', 'options') ): ?>
    <div class="mobile-pub col-sm-6 pub-item pub-size-box">
        <div class="pub-banner" style="background: url(<?php echo get_field('box3', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
    </div>
<?php elseif( $count == 5 && get_field('bigbox1', 'options') ): ?>
    <div class="mobile-pub col-sm-6 pub-item pub-size-bigbox">
        <div class="pub-banner" style="background: url(<?php echo get_field('bigbox1', 'options'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
    </div>
<?php endif; ?>


<div class="post-in-grid with-desc">
    <a class="img-block" href="<?php echo get_permalink(); ?>">
        <figure class="post-thumb">
            <div class="inner" style="background-image:url(<?php echo $thumb; ?>);"></div>
        </figure>
    </a>
    <div class="meta-block">
        <a href="<?php echo get_permalink(); ?>">
            <h3><?php the_title() ?></h3>
            <span class="date-meta"><?php the_time('j F Y') ?></span>
            <p class="excerpt">
                <?php blond_excerpt(155);?>
            </p>
        </a>
        <aside class="metas">
            <a class="author-info" href="<?php echo $author_link; ?>">
                <figure style="background-image:url(<?php echo $photo; ?>);"></figure>
                <span><?php _e('par', 'theme'); ?> <?php echo $name; ?></span>
            </a>
        </aside>
    </div>
</div>
