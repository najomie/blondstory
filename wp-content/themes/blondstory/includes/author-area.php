<?php
    $user_image = get_field( 'photo' );
    $user_desc = get_field( 'description' );
    $user_fname = get_field( 'first_name' );
    $user_lname = get_field( 'last_name' );
    $user_website = get_field( 'website' );
    $user_instagram = get_field( 'instagram' );
    $user_facebook = get_field( 'facebook' );
?>
<div class="collab">
    <div class="authorarea">
        <div class="inner-author">
            <div class="avatar-wrap">
                <img src="<?php echo $user_image; ?> " alt="" />
            </div>
            <div class="authorinfo">
            <h3><?php the_title(); ?></h3>
            <?php if( $user_desc ): ?>
                <p><?php echo $user_desc; ?></p>
            <?php endif; ?>
            <ul class="author-socials">
                <?php if( $user_website ): ?>
                    <li><a class="social" href="<?php echo $user_website; ?>" target="_blank"><i class="fa fa-external-link"></i></a></li>
                <?php endif; ?>
                <?php if( $user_facebook ): ?>
                    <li><a class="social" href="<?php echo $user_facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <?php endif; ?>
                <?php if( $user_instagram): ?>
                    <li><a class="social" href="<?php echo $user_instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <?php endif; ?>
            </ul>
            <a class="more author-link" href="<?php the_permalink(); ?>" rel="author">
                <?php _e( 'Tous les articles de')?> <?php the_title(); ?> <span></span>
            </a>
            </div>
        </div>
    </div>
</div>
