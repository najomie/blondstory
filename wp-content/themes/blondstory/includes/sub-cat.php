<?php
    $category = get_queried_object();
    $parent = get_category($category->category_parent);
    if( $category->category_parent == 0):
        $top_cat = ""; if( empty($top_cat) ): $top_cat = $category->name; endif;
        $terms = get_terms( 'category', array( 'parent' => $category->term_id,'hide_empty' => 0, ) );
    endif;
?>
<?php if( $category->term_id != "" && ! empty($terms) ): ?>
    <div class="small-center-nav inner-cat">
        <ul class="magazine-nav">
            <?php foreach ( $terms as $term ):
                $term_link = get_term_link( $term );
                $cat = get_query_var('cat');
                $current_cat = get_category ($cat);
                if($term->slug == $current_cat->slug): $class = 'current tax'; else: $class = 'tax'; endif;
                echo '<li class="'.$class.'"><a class="'.$class.'" href="' . esc_url( $term_link ) . '"><span>' . $term->name . '</span></a></li>';
            endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
