<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	
	<title><?php wp_title(); ?></title>
	
	<?php wp_head(); ?>
</head>
<body <?php body_class() ?>>

<div class="fullscreen 404" >
	<div class="container">
		<h1><?php _e( 'Erreur 404 !')?></h1>
		<h2><?php _e( 'La page est introuvable')?></h2>
		<div class="cta"><a class="back-to-home" href="<?php bloginfo('wpurl'); ?>"><?php _e( 'Retournez sur l\'accueil')?></a></div>
	</div>
</div>