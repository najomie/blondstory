<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

$image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'shop_catalog' );

?>
<li class="product">

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>


	<figure class="effect-winston">
		<a href="<?php the_permalink(); ?>">
    	<img src="<?php  echo $image[0]; ?>">
		<figcaption>
			<p>
				<span>Voir le produit <i class="fa fa-fw fa-chevron-right"></i></span>
			</p>
		</figcaption>
		</a>
	</figure>
	<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
	<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>

</li>
