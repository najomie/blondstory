<?php
	get_header();

	$largebox       = get_field('largebox1', 'options');
	$largebox_link  = get_field('largebox1_link', 'options');

	$largebox2       = get_field('largebox2', 'options');
	$largebox_link2  = get_field('largebox2_link', 'options');
?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<section class="content">

	<div class="in-container">
		<?php /* Featured posts */  ?>
		<?php get_template_part('includes/home/featured'); ?>
	</div>

	<?php if( $largebox ): ?>
		<a class="largebox" href="<?php echo $largebox_link; ?>" target="_blank"><img src="<?php echo $largebox['url']; ?>" alt="<?php echo $largebox['alt']; ?>"></a>
	<?php endif; ?>

	<div class="in-container">
		<?php /* Recent posts */  ?>
		<?php get_template_part('includes/home/recent'); ?>
	</div>

	<div class="full-container">
		<?php /* Videos */  ?>
		<?php get_template_part('includes/home/video-grid'); ?>
	</div>

	<?php if( $largebox2 ): ?>
		<a class="largebox" href="<?php echo $largebox_link2; ?>" target="_blank"><img src="<?php echo $largebox2['url']; ?>" alt="<?php echo $largebox2['alt']; ?>"></a>
	<?php endif; ?>

	<div class="in-container">
		<?php /* Recettes */  ?>
		<?php get_template_part('includes/home/recettes'); ?>
	</div>

	<div class="full-container">
		<?php /* Outils */  ?>
		<?php get_template_part('includes/home/outils'); ?>
	</div>

</section>
<?php endwhile; endif; ?>

<?php get_footer();?>
