<?php get_header(); ?>

<?php

$posts = get_posts(array(
	'posts_per_page'	=> 12,
	'meta_query' => array(
		array(
			'key' => 'collab_single',
			'value' => '"' . get_the_ID() . '"',
			'compare' => 'LIKE'
		)
	)
));

?>

<section class="content container">

	<div class="row row-grid">

		<aside class="col-sm-push-8 col-sm-4 sidebar">
			<div class="inner">
				<?php get_template_part('includes/pub-sidebar'); ?>
				<?php dynamic_sidebar( 'blog-sidebar' ); ?>
			</div>
		</aside>
		<div class="post-list-third archive-list col-sm-pull-4 col-sm-8">

			<div class="title-suggested">
				<h2><?php _e( 'Tous les articles de : ')?> <span> <?php the_title(); ?></span></h2>
			</div>

			<?php
			    $user_image = get_field( 'photo' );
			    $user_desc = get_field( 'description' );
			    $user_fname = get_field( 'first_name' );
			    $user_lname = get_field( 'last_name' );
			    $user_website = get_field( 'website' );
			    $user_instagram = get_field( 'instagram' );
			    $user_facebook = get_field( 'facebook' );
			?>
			<div class="collab">
			    <div class="authorarea">
			        <div class="inner-author">
			            <div class="avatar-wrap">
			                <img src="<?php echo $user_image; ?> " alt="" />
			            </div>
			            <div class="authorinfo">
			            <?php if( $user_desc ): ?>
			                <p><?php echo $user_desc; ?></p>
			            <?php endif; ?>
			            <ul class="author-socials">
			                <?php if( $user_website ): ?>
			                    <li><a class="social" href="<?php echo $user_website; ?>" target="_blank"><i class="fa fa-external-link"></i></a></li>
			                <?php endif; ?>
			                <?php if( $user_facebook ): ?>
			                    <li><a class="social" href="<?php echo $user_facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                <?php endif; ?>
			                <?php if( $user_instagram): ?>
			                    <li><a class="social" href="<?php echo $user_instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
			                <?php endif; ?>
			            </ul>
			            </div>
			        </div>
			    </div>
			</div>


			<?php get_template_part('includes/sub-cat'); ?>

			<div class="post-grid">

				<?php if( $posts ): $count = 0; foreach( $posts as $post ): $count ++; setup_postdata( $post ); ?>

					<?php include(locate_template('includes/list-post.php')); ?>

				<?php wp_reset_query(); ?>
				<?php endforeach; endif; ?>
			</div>
			<?php echo pagination(); ?>
		</div>

	</div>
</section>

<?php wp_reset_query(); ?>

<?php get_footer();
